import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductServiceService } from '../services/product-service.service';

@Component({
  selector: 'app-app-add-product-form',
  templateUrl: './app-add-product-form.component.html',
  styleUrls: ['./app-add-product-form.component.css']
})
export class AppAddProductFormComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<AppAddProductFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private productService: ProductServiceService) { }

  ngOnInit(): void {
  }
  AddProductToCompany(event:any){
    let addProductCommand = {
      ParentCompanyId: this.data['ParentCompanyId'],
      ParentCategoryId: this.data['ParentCategoryId'],
      Product: event
    };
    this.productService.AddProduct(addProductCommand);
  }
}
