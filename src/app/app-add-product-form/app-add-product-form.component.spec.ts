import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppAddProductFormComponent } from './app-add-product-form.component';

describe('AppAddProductFormComponent', () => {
  let component: AppAddProductFormComponent;
  let fixture: ComponentFixture<AppAddProductFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppAddProductFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppAddProductFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
