import { Component, OnInit } from '@angular/core';
import { AppSingupFormComponent } from '../app-singup-form/app-singup-form.component';
import {MatDialog} from "@angular/material/dialog";
import { CommonServiceService } from '../services/common-service.service';
import { Subscription } from 'rxjs';
import { AppSignInFormComponent } from '../app-sign-in-form/app-sign-in-form.component';
import { AppSignUpAsCompanyComponent } from '../app-sign-up-as-company/app-sign-up-as-company.component';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-app-navbar',
  templateUrl: './app-navbar.component.html',
  styleUrls: ['./app-navbar.component.css']
})
export class AppNavbarComponent implements OnInit {
  routes = [];
  subscription: Subscription;
  loggedInUser : any;
  checkUserIsLoggedIn: string;
  constructor(private dialog: MatDialog,
              private router: Router,
              private commonService : CommonServiceService) { }

  ngOnInit(): void {
    this.loggedInUser = null;
    this.checkUserIsLoggedIn =  localStorage.getItem('user');
    debugger;
    if(this.checkUserIsLoggedIn){
      this.loggedInUser = JSON.parse(this.checkUserIsLoggedIn);
    }
    this.subscription = this.commonService.loggedInUser.subscribe(
      (message) => {
        this.loggedInUser = message;
        debugger;
      }
    );
  }

  SignUp(){

    const dialogRef = this.dialog.open(AppSingupFormComponent, {
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        debugger
      }
    });
  }

  SignUpAsCompany(){

    const dialogRef = this.dialog.open(AppSignUpAsCompanyComponent, {
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        debugger
      }
    });
  }

  logOut(){
    localStorage.removeItem('user');
    this.commonService.logOutUser();
    debugger;
    this.router.navigate([``]);
  }



  logIn(){

    const dialogRef = this.dialog.open(AppSignInFormComponent, {
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        debugger
      }
    });
  }

  

}
