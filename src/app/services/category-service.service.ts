import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthserviceService } from './authservice.service';
import { Config } from '../Configs/configs';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryServiceService {

  categoryListSync = new Subject<any>();

  constructor(private httpClient: HttpClient,
    private authService: AuthserviceService) { }
  GetAllCategoryIdWIthProducts(GetAllCategoryIdWIthProductsCommand) : Observable<any> {
    return this.httpClient.post<any>(`${Config.baseUrl}Category/GetAllCategoryIdWIthProducts`,GetAllCategoryIdWIthProductsCommand);
  }
  GetCategoryUsingId(categoryId) : Observable<any> {
    return this.httpClient.get<any>(`${Config.baseUrl}Category/GetCategoryById?categoryId=${categoryId}`);
  }
  GetCategoryUsingIdList(getCategoryByIdListCommand) : Observable<any> {

    return this.httpClient.post<any>(`${Config.baseUrl}Category/GetCategoryByIdList`,getCategoryByIdListCommand);
  }
  AddCategory(addCategoryCommand)
  {
    this.httpClient.post<any>(`${Config.baseUrl}Category/AddCategoryByParentCategoryId`,addCategoryCommand).subscribe(
      (response) =>{
        console.log(response);
        this.categoryListSync.next(true);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
