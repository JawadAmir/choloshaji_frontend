import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../Configs/configs';
import { CommonServiceService } from './common-service.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {

  constructor(private httpClient: HttpClient,
              private commonService : CommonServiceService,
              private router: Router) { }
  LoginUser(loginInfo:any)
  {
    this.httpClient.post<any>(`${Config.baseUrl}Authentication/Login`,loginInfo).subscribe(
      (response) =>{
        console.log(response);
        debugger;
        localStorage.setItem('user', JSON.stringify(response.userEssentialInfo));
         this.commonService.setLoggedInUser(response.userEssentialInfo);
         if(response.isCompany) {
          this.router.navigate([`category/${response.id}/${response.categoryIds[0]}`]);
        }
         debugger;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
