import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Config } from '../Configs/configs';

@Injectable({providedIn: 'root'})
export class FormService {

constructor(private httpClient: HttpClient) { }
  getForm(formname:string): Observable<any> {
    return this.httpClient.get<any>(`${Config.baseUrl}Form/GetFormByFormName?formName=${formname}`);
  }
}
