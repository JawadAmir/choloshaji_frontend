import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../Configs/configs';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  constructor(private httpClient: HttpClient) { }

  AddProduct(addProductCommand)
  {
    this.httpClient.post<any>(`${Config.baseUrl}Product/AddProductUsingParentCategoryId`,addProductCommand).subscribe(
      (response) =>{
        console.log(response);
        debugger;
      },
      (error) => {
        console.log(error);
      }
    );
  }
  GetProductWithParentCategory(GetProductUsingParentCategoryIdCommand) : Observable<any> {
    return this.httpClient.post<any>(`${Config.baseUrl}Product/GetProductUsingParentCategoryId`,GetProductUsingParentCategoryIdCommand);
  }
}
