import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { Config } from '../Configs/configs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  // loggedInUser : BehaviorSubject<any> = new BehaviorSubject<any>({});

  public loggedInUser = new Subject<any>();

  public selectedCategory = new Subject<any>();



  constructor(private httpClient:HttpClient
  ) { }


  setSelectedCategory(user){
   this.selectedCategory.next(user);
  }





  // getLoggedInUser(){
  //   debugger;
  //   this.loggedInUser.next(this.loggedInUser);
  // }
  setLoggedInUser(user){
    debugger;
    this.loggedInUser.next(user);
  }
  logOutUser(){
    debugger;
    this.loggedInUser.next(null);
    debugger
  }
  UploadFile(formFile) : Observable<any>{
      const formData = new FormData();
      formData.append('file', formFile);
      return this.httpClient.post<any>(`${Config.baseUrl}FileUpload/UploadFile`,formData);
  }
  DeleteFile(publicId) : Observable<any>{
    const data = {
      'PublicId' : publicId
    }
    return this.httpClient.post<any>(`${Config.baseUrl}FileUpload/DeleteFile`,data);
}
}
