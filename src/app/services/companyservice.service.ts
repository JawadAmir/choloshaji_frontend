import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthserviceService } from './authservice.service';
import { Config } from '../Configs/configs';

@Injectable({
  providedIn: 'root'
})
export class CompanyserviceService {

  constructor(private httpClient: HttpClient,
    private authService: AuthserviceService) { }

  AddCompany(User:any)
  {
    debugger;
    this.httpClient.post<any>(`${Config.baseUrl}Company/AddCompany`,User).subscribe(
      (response) =>{
        debugger;
        console.log(response);
        let loginInfo = {};
        loginInfo['userName'] = response.userEssentialInfo.userName;
        loginInfo['password'] = response.userEssentialInfo.password;
        this.authService.LoginUser(loginInfo);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
