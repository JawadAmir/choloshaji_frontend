import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Config } from '../Configs/configs';
import { AuthserviceService } from './authservice.service';
import { Router } from '@angular/router';

@Injectable({providedIn: 'root'})
export class UserService {

constructor(private httpClient: HttpClient,
           private authService: AuthserviceService,
           private router: Router,) { }
  AddUser(User:any)
  {
    this.httpClient.post<any>(`${Config.baseUrl}User/AddUser`,User).subscribe(
      (response) =>{
        debugger;
        console.log(response);
        let loginInfo = {};
        loginInfo['userName'] = response.userEssentialInfo.userName;
        loginInfo['password'] = response.userEssentialInfo.password;
        this.authService.LoginUser(loginInfo);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  logIn(User:any)
  {
    debugger;
    this.httpClient.post<any>(`${Config.baseUrl}user/LoginUser`,User).subscribe(
      (response) =>{
        debugger;
        console.log(response);
        if(response.isCompany) {
          this.router.navigate([`category/${response.id}/${response.categoryIds[0]}`]);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }











}
