import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppNavbarComponent } from './app-navbar/app-navbar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import { FlexLayoutModule } from "@angular/flex-layout";
import {MatCardModule} from '@angular/material/card';
import { AppSingupFormComponent } from './app-singup-form/app-singup-form.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { GenericFormComponent } from './generic-form/generic-form.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppSignInFormComponent } from './app-sign-in-form/app-sign-in-form.component';
import { AppSignUpAsCompanyComponent } from './app-sign-up-as-company/app-sign-up-as-company.component';
import { AppShowCategoryComponent } from './app-show-category/app-show-category.component';
import {MatButtonModule} from '@angular/material/button';
import { AppAddCategoryFromComponent } from './app-add-category-from/app-add-category-from.component';
import { RouterModule } from '@angular/router';
import { AppAddProductFormComponent } from './app-add-product-form/app-add-product-form.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import { AppShowProductsComponent } from './app-show-products/app-show-products.component';

@NgModule({
  declarations: [
    AppComponent,
    AppNavbarComponent,
    AppSingupFormComponent,
    GenericFormComponent,
    AppSignInFormComponent,
    AppSignUpAsCompanyComponent,
    AppShowCategoryComponent,
    AppAddCategoryFromComponent,
    AppAddProductFormComponent,
    AppShowProductsComponent
  ],
  imports: [
    BrowserModule,
    MatSidenavModule,
    RouterModule,
    MatButtonModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatCardModule,
    AppRoutingModule,
    FormsModule,
     ReactiveFormsModule,
     MatListModule,
     MatDialogModule,
     HttpClientModule,
     FlexLayoutModule.withConfig({
      useColumnBasisZero: false,
      printWithBreakpoints: ['xs', 'sm', 'md', 'lg', 'xl', 'lt-sm', 'lt-md', 'lt-lg', 'lt-xl', 'gt-xs', 'gt-sm', 'gt-md', 'gt-lg']
  }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
