import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppSignUpAsCompanyComponent } from './app-sign-up-as-company.component';

describe('AppSignUpAsCompanyComponent', () => {
  let component: AppSignUpAsCompanyComponent;
  let fixture: ComponentFixture<AppSignUpAsCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppSignUpAsCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppSignUpAsCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
