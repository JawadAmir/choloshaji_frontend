import { Component, OnInit } from '@angular/core';
import { UserService } from './../services/userservice';
import { CompanyserviceService } from '../services/companyservice.service';

@Component({
  selector: 'app-app-sign-up-as-company',
  templateUrl: './app-sign-up-as-company.component.html',
  styleUrls: ['./app-sign-up-as-company.component.css']
})
export class AppSignUpAsCompanyComponent implements OnInit {

  constructor( private companyService: CompanyserviceService) { }

  ngOnInit(): void {
  }

  AddCompany(event:any){
    this.companyService.AddCompany(event);
  }

}
