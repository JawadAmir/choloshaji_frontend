import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppShowCategoryComponent } from './app-show-category/app-show-category.component';
import { AppNavbarComponent } from './app-navbar/app-navbar.component';


export const routes: Routes = [
  // { path: '', redirectTo: '/category',component: AppNavbarComponent,pathMatch: 'full' }, 
  // {path: 'category/:id' , component: AppShowCategoryComponent}
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
   { path: 'home', component: AppNavbarComponent },
  { path: 'category', component: AppShowCategoryComponent,},
  { path: 'category/:companyIdForCategory/:rootCategoryId', component: AppShowCategoryComponent },
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
