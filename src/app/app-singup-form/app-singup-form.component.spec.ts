import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppSingupFormComponent } from './app-singup-form.component';

describe('AppSingupFormComponent', () => {
  let component: AppSingupFormComponent;
  let fixture: ComponentFixture<AppSingupFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppSingupFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppSingupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
