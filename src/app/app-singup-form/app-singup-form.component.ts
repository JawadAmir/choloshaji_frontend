import { FormGroup, FormControl } from '@angular/forms';
import { Component, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { UserService } from '../services/userservice';
// import {DialogData} from '../../../root/notice/notice.component';

@Component({
  selector: 'app-app-singup-form',
  templateUrl: './app-singup-form.component.html',
  styleUrls: ['./app-singup-form.component.css']
})



export class AppSingupFormComponent {

  constructor(
    public dialogRef: MatDialogRef<AppSingupFormComponent>,
    public userService: UserService
    // @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
  }

  // registerForm = new FormGroup({
  //   userName: new FormControl(''),
  //   password: new FormControl(''),
  //   confirmPassword: new FormControl(''),
  //   phoneNumber: new FormControl(''),

  //   // form grouping / nested form groups
  //   address: new FormGroup({
  //     city: new FormControl(''),
  //     phoneNumber: new FormControl(''),
  //     state: new FormControl(''),
  //     postCode: new FormControl('')
  //   })
  // });

//  loadApiData() {
//    this.registerForm.setValue({
//      userName: 'Bruce Wayne',
//      password: 'test123',
//      confirmPassword: 'test123',
//      address: {
//        city: 'City',
//        state: 'State',
//        postCode: '4215'
//      }
//    });
//   }

  RegisterTheUser(event:any){
    this.userService.AddUser(event);
  }
}
