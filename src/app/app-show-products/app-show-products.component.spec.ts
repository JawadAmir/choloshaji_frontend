import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppShowProductsComponent } from './app-show-products.component';

describe('AppShowProductsComponent', () => {
  let component: AppShowProductsComponent;
  let fixture: ComponentFixture<AppShowProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppShowProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppShowProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
