import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { CategoryServiceService } from '../services/category-service.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';
import { ProductServiceService } from '../services/product-service.service';
import { CommonServiceService } from '../services/common-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-app-show-products',
  templateUrl: './app-show-products.component.html',
  styleUrls: ['./app-show-products.component.scss']
})
export class AppShowProductsComponent implements OnInit,OnDestroy {
  parentCategoryId : string;

  constructor(private categoryService:CategoryServiceService,
    private productService: ProductServiceService,
    private commonService: CommonServiceService,
    private activatedRoute : ActivatedRoute,
    private  router : Router) { }
  @Input() parentCategory: any;
  url : any;
  unsubscribe$ = new Subject();
  allCategoryIdsWithProduct : any;
  products: any;
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  ngOnInit(): void {
    this.parentCategoryId = window.location.href.slice(64,96);

    this.products = [];
    this.activatedRoute.url.subscribe(url =>{
    console.log(url);
    console.log(this.parentCategory);
    if(url[2].path!==this.parentCategoryId){
      this.products  = [];
      this.setProductForOneCategory(url[2].path);
    }
    else{
      this.products  = [];
      this.setAllCategoryIdWIthProducts(this.parentCategoryId);
    }
   });

  }

  setAllCategoryIdWIthProducts(z){
    if(this.parentCategory){
      let GetAllCategoryIdWIthProductsCommand = {
       ParentCategoryId: z
      }
      this.categoryService.GetAllCategoryIdWIthProducts(GetAllCategoryIdWIthProductsCommand)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(response =>{
        if(response){
          this.allCategoryIdsWithProduct = response;
          this.setProducts();
        }
      });
    }
  }
  setProducts(){
    if(this.allCategoryIdsWithProduct){
      this.allCategoryIdsWithProduct.forEach(id => {
        this.setProductForOneCategory(id);
      });
    }
  }
  setProductForOneCategory(id){
    let GetProductUsingParentCategoryIdCommand = {
      ParentCategoryId: id
     }
     this.productService.GetProductWithParentCategory(GetProductUsingParentCategoryIdCommand)
     .pipe(takeUntil(this.unsubscribe$)).subscribe(response =>{
       if(response){
         this.products = this.products.concat(response);
       }
     });
  }
}
