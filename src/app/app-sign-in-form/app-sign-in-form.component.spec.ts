import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppSignInFormComponent } from './app-sign-in-form.component';

describe('AppSignInFormComponent', () => {
  let component: AppSignInFormComponent;
  let fixture: ComponentFixture<AppSignInFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppSignInFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppSignInFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
