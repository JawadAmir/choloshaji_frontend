import { Component, OnInit } from '@angular/core';
import { UserService } from './../services/userservice';
import { AuthserviceService } from '../services/authservice.service';

@Component({
  selector: 'app-app-sign-in-form',
  templateUrl: './app-sign-in-form.component.html',
  styleUrls: ['./app-sign-in-form.component.css']
})
export class AppSignInFormComponent implements OnInit {

  constructor( private userService : UserService,
    private authServce: AuthserviceService) { }

  ngOnInit(): void {
  }

  loginUer(event:any){
    this.authServce.LoginUser(event);
  }

}
