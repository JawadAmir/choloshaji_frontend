import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppShowCategoryComponent } from './app-show-category.component';

describe('AppShowCategoryComponent', () => {
  let component: AppShowCategoryComponent;
  let fixture: ComponentFixture<AppShowCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppShowCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppShowCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
