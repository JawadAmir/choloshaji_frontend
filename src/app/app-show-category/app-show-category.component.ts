import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppAddCategoryFromComponent } from '../app-add-category-from/app-add-category-from.component';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil } from 'rxjs/internal/operators';
import { Subject } from 'rxjs';
import { CategoryServiceService } from '../services/category-service.service';
import { AuthserviceService } from '../services/authservice.service';
import { CommonServiceService } from '../services/common-service.service';
import { HttpClient } from '@angular/common/http';
import { Config } from '../Configs/configs';
import { AppAddProductFormComponent } from '../app-add-product-form/app-add-product-form.component';

@Component({
  selector: 'app-app-show-category',
  templateUrl: './app-show-category.component.html',
  styleUrls: ['./app-show-category.component.css']
})
export class AppShowCategoryComponent implements OnInit,OnDestroy {
 companyId: string;
 currentCategory: any;
 unsubscribe$ = new Subject();
 currentSubCategoris : any=[];
  loggedInUser: any;
  subscription: any;
  constructor(private dialog: MatDialog,
    private commonService : CommonServiceService,
    private activatedRoute: ActivatedRoute,
    private categoryService: CategoryServiceService,
    private httpClient: HttpClient,
    private router: Router) { }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    debugger;
    this.activatedRoute.params.pipe(takeUntil(this.unsubscribe$)).subscribe(params => {
      console.log("==== PARAMS CHANGED ====");
      if (params['companyIdForCategory'] && params['rootCategoryId']) {
        this.companyId = params['companyIdForCategory'];
        let currentCategoryId = params['rootCategoryId'];
        this.syncCategoryWithCompany(currentCategoryId);
      }
    });
    this.categoryService.categoryListSync.pipe(takeUntil(this.unsubscribe$)).subscribe(response =>
      {
        if(response)
        {
          this.setCurrentSubCategories(this.currentCategory.id);
        }
    });

    const a =  localStorage.getItem('user');
    debugger;
    if(a){
      this.loggedInUser = JSON.parse(a);
      console.log(this.loggedInUser.userName);
      debugger
    }
      this.subscription = this.commonService.loggedInUser.subscribe(
        (message) => {
          debugger;
          this.loggedInUser = message;
          debugger;
        }
      );


  }

  syncCategoryWithCompany(currentCategoryId){
    this.categoryService.GetCategoryUsingId(currentCategoryId).pipe(takeUntil(this.unsubscribe$)).subscribe(category => {
      if (category) {
        console.log('items:', category);
        this.currentCategory = category;
        this.setCurrentSubCategories(category.id);
      }
    });
  }

  setCurrentSubCategories(parentCategoryId){
    let getCategoryByIdListCommand = {
      'ParentCategoryId': parentCategoryId
    };
    this.categoryService.GetCategoryUsingIdList(getCategoryByIdListCommand).pipe(takeUntil(this.unsubscribe$)).subscribe(categoryList => {
      if (categoryList) {
        console.log('items:', categoryList);
        this.currentSubCategoris = categoryList;
      }
    });
  }

  loadCurrentSubCategory(selectedSubCategory){
    debugger;
    this.router.navigate([`category/${this.companyId}/${selectedSubCategory.id}`]);
    this.commonService.setSelectedCategory(selectedSubCategory);
  }

  addCategory(){
      const dialogRef = this.dialog.open(AppAddCategoryFromComponent, {
        data :{
          'ParentCategoryId': this.currentCategory['id'],
          'ParentCompanyId': this.companyId
          }
      });
      dialogRef.afterClosed().subscribe((result: any) => {
        if (result) {
          debugger
        }
      });

  }
  addProduct(){
    const dialogRef = this.dialog.open(AppAddProductFormComponent, {
      data :{
        'ParentCategoryId': this.currentCategory['id'],
        'ParentCompanyId': this.companyId
        }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        debugger
      }
    });
  }
  getDefaultFile(files) {
    if (files.length > 0) {
      return files[0];
    }
  }
  onFileSelect(event) {
    if(event.target.files && event.target.files.length >0){
      console.log(this.getDefaultFile(event.target.files));
      var file = this.getDefaultFile(event.target.files);
      debugger;
      const formData = new FormData();
      formData.append('file', file);
      this.httpClient.post<any>(`${Config.baseUrl}FileUpload/UploadFile`,formData).subscribe(response => {
        console.log(response);
      });
    }
  }

  logOut(){
    localStorage.removeItem('user');
    this.commonService.logOutUser();
    debugger;
    this.router.navigate([``]);
  }
}
