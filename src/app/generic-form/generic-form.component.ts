import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FormService } from '../services/formservice';
import { FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/internal/operators';
import { Subject } from 'rxjs';
import { element, Key } from 'protractor';
import { HttpClient } from '@angular/common/http';
import { Config } from '../Configs/configs';
import { CommonServiceService } from '../services/common-service.service';

@Component({
  selector: 'app-generic-form',
  templateUrl: './generic-form.component.html',
  styleUrls: ['./generic-form.component.scss']
})
export class GenericFormComponent implements OnInit , OnDestroy{
  unSubscribe$ = new Subject();
  fileInputs = {};
  constructor(private formservice : FormService,
              private _formBuilder: FormBuilder,
              private commonService:CommonServiceService,
              private httpClient: HttpClient, ) { }
  fieldDefinations:any;
  form: any;
  @Input() formName: any;
  @Input() formFileName: any;
  @Output() filledupForm = new EventEmitter<any>();


  ngOnInit() {
    debugger;
    // if(!this.formFileName)
    // {
    //   this.formFileName = 'usersignupforms';
    //   this.formName = 'Register'
    // }
    this.formservice.getForm(this.formFileName).pipe(takeUntil(this.unSubscribe$)).subscribe( form =>{
      if(form){
        this.fieldDefinations = form.fieldDefinitions;
        this.createForm();
      }
    });
  }
  createForm(){
    this.form = this._formBuilder.group(this.getFormObject());
    console.log(this.form);
  }
  getFormObject(){
    let formObject:any = {};
    this.fieldDefinations.forEach((element: any) => {
      if(formObject[element.key] == null || formObject[element.key] == undefined) {
        formObject[element.key] = [null];
        if(element.isRequired){
          formObject[element.key].push(Validators.required);
        }
      }
    });
    return formObject;
  }
  onSubmit(){
    debugger;
    let item = Object.assign({}, this.form.value);
    let fileInputFieldDefinations = this.fieldDefinations.filter(o => o.type === 'File');
    fileInputFieldDefinations.forEach(element => {
      item[element.key] = this.fileInputs[element.key];
    });
    this.filledupForm.emit(item);
  }
  checkValidityForField(key:any){
    if(this.form.controls[key].touched && this.form.controls[key].status === 'INVALID'){
      return true;
    }
    return false;
  }
  checkIsRequired(key:any){
    if(this.form.controls[key].errors.required){
      return true;
    }
    return false;
  }
  getDefaultFile(files) {
    if (files.length > 0) {
      return files[0];
    }
  }
  onFileSelect(event,key) {
    if(event.target.files && event.target.files.length >0){
      console.log(this.getDefaultFile(event.target.files));
      var file = this.getDefaultFile(event.target.files);
      this.commonService.UploadFile(file).subscribe(response => {
        console.log(response);
        if(this.fileInputs[key]) {
          this.commonService.DeleteFile(this.fileInputs[key].PublicId).subscribe(response =>{
            if(response){
            }
          });
        }
        let fileModel = {
          'Name' : file.name,
          'PublicId' : response.publicId,
          'Url':response.url
        }
        this.fileInputs[key] = fileModel;
      });
    }
  }
  doesExistFileOnKey(key){
    if(this.fileInputs[key]) {
      return true;
    }
    return false;
  }
  ngOnDestroy(){
    this.unSubscribe$.next(false);
    this.unSubscribe$.unsubscribe();
  }

}
