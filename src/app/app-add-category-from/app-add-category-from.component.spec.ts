import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppAddCategoryFromComponent } from './app-add-category-from.component';

describe('AppAddCategoryFromComponent', () => {
  let component: AppAddCategoryFromComponent;
  let fixture: ComponentFixture<AppAddCategoryFromComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppAddCategoryFromComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppAddCategoryFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
