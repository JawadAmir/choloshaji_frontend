import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../services/userservice';
import { CategoryServiceService } from '../services/category-service.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-app-add-category-from',
  templateUrl: './app-add-category-from.component.html',
  styleUrls: ['./app-add-category-from.component.css']
})
export class AppAddCategoryFromComponent implements OnInit {
  parentCategoryId: any;
  constructor( private categoryService : CategoryServiceService,
    public dialogRef: MatDialogRef<AppAddCategoryFromComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  AddCategoryToCompany(event:any){
    let addCategoryCommand = {
      ParentCompanyId: this.data['ParentCompanyId'],
      ParentCategoryId: this.data['ParentCategoryId'],
      CategoryName: event['CategoryName']
    }
    this.categoryService.AddCategory(addCategoryCommand);
  }

}
